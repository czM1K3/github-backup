import fetch from "node-fetch";
import writeYamlFile from "write-yaml-file";
import * as config from "./config.js";

(async () => {
	/**
	 * @type {Array<{
	 * 		name: string,
	 * 		fill_name: string,
	 * }>}
	 */
	const arr = [];
	let pass = 1;
	while(true) {
		console.log(`Fetching for 100 repositories.Pass ${pass}`);
		const raw = await fetch(`https://api.github.com/user/repos?per_page=100&page=${pass++}`, {
			headers: {
				"Authorization": `token ${config.gitHubToken}`
			}
		});
		console.log(`Fetched successfully.`);
		/**
		 * @type {Array<{
		 * 		name: string,
		 * 		fill_name: string,
		 * }>}
		 */
		const data = await raw.json();
		if(data.length === 0) break;
		arr.push(...data);
		if (data.length < 50) break;
	}
	const repos = arr.map((x) => ({name: x.full_name, url: x.ssh_url}));
	await writeYamlFile("list.yaml", {
		repos,
		backupPath: config.backupPath,
	});
})();